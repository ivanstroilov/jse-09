package ru.t1.stroilov.tm.constant;

public final class AppConstant {

    public static final String VERSION = "version";

    public static final String HELP = "help";

    public static final String INFO = "info";

    public static final String EXIT = "exit";

    public static final String COMMANDS = "commands";

    public static final String ARGUMENTS = "arguments";

    public static final String PROJECT_LIST = "project-list";

    public static final String PROJECT_CREATE = "project-create";

    public static final String PROJECT_DELETE = "project-delete";

    public static final String PROJECT_SHOW_BY_ID = "project-show-by-id";

    public static final String PROJECT_SHOW_BY_INDEX = "project-show-by-index";

    public static final String PROJECT_UPDATE_BY_ID = "project-update-by-id";

    public static final String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    public static final String PROJECT_DELETE_BY_ID = "project-delete-by-id";

    public static final String PROJECT_DELETE_BY_INDEX = "project-delete-by-index";

    public static final String TASK_LIST = "task-list";

    public static final String TASK_CREATE = "task-create";

    public static final String TASK_DELETE = "task-delete";

    public static final String TASK_SHOW_BY_ID = "task-show-by-id";

    public static final String TASK_SHOW_BY_INDEX = "task-show-by-index";

    public static final String TASK_SHOW_BY_PROJECT_ID = "task-show-by-project-id";

    public static final String TASK_UPDATE_BY_ID = "task-update-by-id";

    public static final String TASK_UPDATE_BY_INDEX = "task-update-by-index";

    public static final String TASK_DELETE_BY_ID = "task-delete-by-id";

    public static final String TASK_DELETE_BY_INDEX = "task-delete-by-index";

    public static final String TASK_BIND_TO_PROJECT = "task-bind-to-project";

    public static final String TASK_UNBIND_FROM_PROJECT = "task-unbind-from-project";

}