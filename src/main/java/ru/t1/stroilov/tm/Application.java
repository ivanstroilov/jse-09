package ru.t1.stroilov.tm;

import ru.t1.stroilov.tm.api.component.IBootstrap;
import ru.t1.stroilov.tm.component.Bootstrap;

public class Application {

    public static void main(String... args) {
        IBootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}