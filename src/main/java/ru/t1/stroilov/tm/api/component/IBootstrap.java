package ru.t1.stroilov.tm.api.component;

public interface IBootstrap {

    void run(String... args);

}