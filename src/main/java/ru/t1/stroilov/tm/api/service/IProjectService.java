package ru.t1.stroilov.tm.api.service;

import ru.t1.stroilov.tm.api.repository.IProjectRepository;
import ru.t1.stroilov.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name);

    Project create(String name, String description);

    Project updateByID(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

}
