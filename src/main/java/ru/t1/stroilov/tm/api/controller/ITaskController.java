package ru.t1.stroilov.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void createTask();

    void clearTasks();

    void removeTaskByID();

    void removeTaskByIndex();

    void showTaskByID();

    void showTaskByProjectID();

    void showTaskByIndex();

    void updateTaskByID();

    void updateTaskByIndex();

}
